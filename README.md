# OpenML dataset: house_16H

https://www.openml.org/d/44123

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dataset used in the tabular data benchmark https://github.com/LeoGrin/tabular-benchmark, transformed in the same way. This dataset belongs to the "classification on numerical features" benchmark. Original description: 
 
**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Binarized version of the original data set (see version 1). It converts the numeric target feature to a two-class nominal target feature by computing the mean and classifying all instances with a lower target value as positive ('P') and all others as negative ('N').

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44123) of an [OpenML dataset](https://www.openml.org/d/44123). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44123/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44123/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44123/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

